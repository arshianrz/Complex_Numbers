#ifndef IMAGINARYNUMS_H
#define IMAGINARYNUMS_H


class Complex
{
private:
    float real_;
    float imaginary_;

public:

    Complex(Complex&);
    Complex();

    Complex operator - (Complex&);
    Complex operator + (Complex&);
    Complex operator / (Complex&);
    bool operator ==(Complex&);

    void input();
    void print();
    void run();

    float getReal() const;
    float getImaginary() const;
};

#endif // IMAGINARYNUMS_H
