#include "imaginarynums.h"
#include <iostream>

using namespace std;

Complex::Complex()
{
    real_=0;
    imaginary_=0;
}
Complex::Complex(Complex &)
{}

float Complex::getImaginary() const
{
    return imaginary_;
}

float Complex::getReal() const
{
    return real_;
}


//----------------------------------------------***END of Setters & Getters***-------------------------------------------------------

Complex Complex::operator- (Complex &c1)
{

        Complex temp1;
        temp1.real_ = (real_) - (c1.real_);
        temp1.imaginary_ = (imaginary_) - (c1.imaginary_);
        return temp1;

}

Complex Complex::operator+ (Complex &c2)
{
        Complex temp2;
        temp2.real_ = (real_) + (c2.real_);
        temp2.imaginary_ = (imaginary_) + (c2.imaginary_);
        return temp2;
}

Complex Complex::operator/ (Complex &c3)
{
     Complex temp3;
     temp3.real_=((real_*c3.real_)+(imaginary_*c3.imaginary_))/((c3.real_*c3.real_)+(c3.imaginary_*c3.imaginary_));
     temp3.imaginary_=((imaginary_*c3.real_)-(real_*c3.imaginary_))/((c3.real_*c3.real_)+(c3.imaginary_*c3.imaginary_));
     return temp3;
}

bool Complex::operator== (Complex &c4)
{

    if((real_ == c4.real_ && imaginary_==c4.imaginary_))
        return true;
    else
        return false;
}

//*********************************************************************************************************************************

void Complex::input()
{
    cout << "Enter real and imaginary sections : \n\n";
    cout<<"The real part is :";
    cin>>real_;
    cout<<"The imaginary part is :";
    cin >> imaginary_;
    cout<<"\n\n";
}

void Complex::print()
{
    cout << real_;
    if (imaginary_>0)
        cout<<'+'<<imaginary_<<'i'<<endl;

    else if (imaginary_<0)
        cout <<imaginary_<<'i'<<endl;
}

void Complex::run()
{
    Complex clocal,c2local,answer;


    char ans='y';
    char opt;
    cout<<"***************************************************************\n";
    cout<<"Enter first complex:\n";
    clocal.input();

    cout<<"Enter second complex:\n";
    c2local.input();
    cout<<endl;


    while(ans=='y')
    {
        cout<<"what kind of operation do you want? (+,-,/)\n(Note:If you want to compare the numbers press '=' ) \n";
        cin>>opt;

        switch (opt)
        {
        case '+':

            answer=clocal+c2local;
            answer.print();
            break;


        case '-':

           answer=clocal-c2local;
           answer.print();
           break;

        case '/':

            answer=clocal/c2local;
            answer.print();
            break;

        case '=':
         {
             if (clocal==c2local)
             {
                 cout<<"They are equal.\n";
             }
             else
             {
                 cout<<"They are not equal.\n";
             }
             break;
        }

        default:

            break;
        }

        cout<<"Do you want to calculate again?(y/n) \n";
        cin>>ans;
        cout<<endl;
    }

}



